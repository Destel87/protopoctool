#ifndef CONSOLEWINDOW_H
#define CONSOLEWINDOW_H

#include <QtCore/QtGlobal>
#include <QMainWindow>
#include <QtSerialPort/QSerialPort>

QT_BEGIN_NAMESPACE

class QLabel;

namespace Ui {
class ConsoleWindow;
}

class Console;
class SettingsDialog;

class ConsoleWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit ConsoleWindow(QWidget *parent = nullptr);
        ~ConsoleWindow();

        void setActive(bool bVal);
        bool getActive(void);

    private slots:
        void openSerialPort();
        void closeSerialPort();
        void about();
        void writeData(const QByteArray &data);
        void readData();

        void handleError(QSerialPort::SerialPortError error);

    private:
        void initActionsConnections();

    private:
        void showStatusMessage(const QString &message);

    private:
        Ui::ConsoleWindow *ui;

        QLabel *status;
        Console *console;
        SettingsDialog *settings;
        QSerialPort *serial;

        bool _active;
};

#endif // CONSOLEWINDOW_H
