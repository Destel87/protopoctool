#ifndef ETHTCPSOCKET_H
#define ETHTCPSOCKET_H

#include <QObject>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QDebug>

class EthTcpSocket : public QObject
{
    Q_OBJECT
public:
    EthTcpSocket();

    void doConnect(QString sIpAddServer, quint16 uiPortNumber);
    void closeConnection();
    void sendData(QByteArray array);
    void readData(QByteArray &array);

signals:
    void dataReady();

public slots:
    void connected();
    void disconnected();
    void bytesWritten(qint64 bytes);
    void readyRead();

private:
    QTcpSocket *socket;
    QByteArray _appoRxData;
};

#endif // ETHTCPSOCKET_H
