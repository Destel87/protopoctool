#include "EthTcpSocket.h"

EthTcpSocket::EthTcpSocket()
{

}

void EthTcpSocket::doConnect(QString sIpAddServer, quint16 uiPortNumber)
{
    socket = new QTcpSocket();

    connect(socket, SIGNAL(connected()),this, SLOT(connected()));
    connect(socket, SIGNAL(disconnected()),this, SLOT(disconnected()));
    connect(socket, SIGNAL(bytesWritten(qint64)),this, SLOT(bytesWritten(qint64)));
    connect(socket, SIGNAL(readyRead()),this, SLOT(readyRead()));
    connect(socket, SIGNAL(readyRead()),this, SIGNAL(dataReady()));

    qDebug() << "connecting...";

    // this is not blocking call
//    socket->connectToHost("127.0.0.1", 9000);
    socket->connectToHost(sIpAddServer, uiPortNumber);

    // we need to wait...
    if(!socket->waitForConnected(5000))
    {
        qDebug() << "Error: " << socket->errorString();
    }
}

void EthTcpSocket::closeConnection()
{
    socket->disconnectFromHost();
}

void EthTcpSocket::connected()
{
    qDebug() << "connected...";

    // Hey server, tell me about you.
//    socket->write("HEAD / HTTP/1.0\r\n\r\n\r\n\r\n");
}

void EthTcpSocket::disconnected()
{
    qDebug() << "disconnected...";
}

void EthTcpSocket::bytesWritten(qint64 bytes)
{
    qDebug() << bytes << " bytes written...";
}

void EthTcpSocket::readyRead()
{
    qDebug() << "reading...";

    // read the data from the socket
    _appoRxData.clear();
    _appoRxData = socket->readAll();
    qDebug() << _appoRxData;

}

void EthTcpSocket::readData(QByteArray &array)
{
   array =  _appoRxData;
}

void EthTcpSocket::sendData(QByteArray array)
{
    socket->write(array);

    qDebug() << array;
}
