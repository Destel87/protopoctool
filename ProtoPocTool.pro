QT       += core gui
QT       += network
QT       += widgets serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += $$PWD/Utility/
INCLUDEPATH += $$PWD/Console/
INCLUDEPATH += $$PWD/Network/
INCLUDEPATH += $$PWD/Protocol/

SOURCES += \
    Console/console.cpp \
    Console/consolewindow.cpp \
    Console/settingsdialog.cpp \
    Network/EthTcpSocket.cpp \
    Protocol/TemperatureDialog.cpp \
    Protocol/qcustomplot.cpp \
    Protocol/tempThread.cpp \
    Protocol/textresultwindow.cpp \
    Protocol/voltagedialog.cpp \
    Protocol/voltagethread.cpp \
    Utility/crc16.c \
    Utility/crc32.c \
    Utility/swap.c \
    main.cpp \
    Protocol/mainwindow.cpp \
    Protocol/mainwindowProto.cpp \
    Protocol/password.cpp

HEADERS += \
    Console/console.h \
    Console/consolewindow.h \
    Console/settingsdialog.h \
    Network/EthTcpSocket.h \
    Protocol/TemperatureDialog.h \
    Protocol/qcustomplot.h \
    Protocol/tempThread.h \
    Protocol/textresultwindow.h \
    Protocol/voltagedialog.h \
    Protocol/voltagethread.h \
    Utility/crc16.h \
    Utility/crc32.h \
    Utility/swap.h \
    Protocol/EthProtoInclude.h \
    Protocol/mainwindow.h \
    Protocol/password.h \
    version.h

FORMS += \
    Console/consolewindow.ui \
    Console/settingsdialog.ui \
    Protocol/mainwindow.ui \
    Protocol/password.ui \
    Protocol/temperaturedialog.ui \
    Protocol/textresultwindow.ui \
    Protocol/voltagedialog.ui

TRANSLATIONS += \
    ProtoPocTool_en_150.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    Protocol/application.qrc \
    Console/terminal.qrc

DISTFILES += \
    Protocol/audio-stop.png
