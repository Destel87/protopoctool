#ifndef ETHPROTOINCLUDE_H
#define ETHPROTOINCLUDE_H



#define   ETH_PROTO_TCP_WAIT_TIME       100 //ms
#define   ETH_PROTO_TEST_RESULT_OK      "OK"
#define   ETH_PROTO_TEST_RESULT_NOK    "NOK"

//Command
#define   ETH_PROTO_TCP_CMD_REL         0x40
#define   ETH_PROTO_TCP_CMD_PM          0x41
#define   ETH_PROTO_TCP_CMD_FAN         0x42
#define   ETH_PROTO_TCP_CMD_LED         0x43
#define   ETH_PROTO_TCP_CMD_CAN         0x44
#define   ETH_PROTO_TCP_CMD_NSH_R       0x45
#define   ETH_PROTO_TCP_CMD_NSH_M       0x46
#define   ETH_PROTO_TCP_CMD_AUDIO       0x47
#define   ETH_PROTO_TCP_CMD_NTC         0x48
#define   ETH_PROTO_TCP_CMD_CAMERA      0x49
#define   ETH_PROTO_TCP_CMD_SERIAL      0x4A
#define   ETH_PROTO_TCP_CMD_EEPROM      0x4B
#define   ETH_PROTO_TCP_CMD_PWMDEVICE   0x4C
#define   ETH_PROTO_TCP_CMD_CONN        0x4D
#define   ETH_PROTO_TCP_CMD_GPIO        0x4E
#define   ETH_PROTO_TCP_CMD_TEC         0x4F
#define   ETH_PROTO_TCP_CMD_REL         0x40

//Answer
#define   ETH_PROTO_TCP_ANS_REL         0x60
#define   ETH_PROTO_TCP_ANS_PM          0x61
#define   ETH_PROTO_TCP_ANS_FAN         0x62
#define   ETH_PROTO_TCP_ANS_LED         0x63
#define   ETH_PROTO_TCP_ANS_CAN         0x64
#define   ETH_PROTO_TCP_ANS_NSH_R       0x65
#define   ETH_PROTO_TCP_ANS_NSH_M       0x66
#define   ETH_PROTO_TCP_ANS_AUDIO       0x67
#define   ETH_PROTO_TCP_ANS_NTC         0x68
#define   ETH_PROTO_TCP_ANS_CAMERA      0x69
#define   ETH_PROTO_TCP_ANS_SERIAL      0x6A
#define   ETH_PROTO_TCP_ANS_EEPROM      0x6B
#define   ETH_PROTO_TCP_ANS_PWMDEVICE   0x6C
#define   ETH_PROTO_TCP_ANS_CONN        0x6D
#define   ETH_PROTO_TCP_ANS_TEC         0x6F
#define   ETH_PROTO_TCP_ANS_GPIO        0x6E

//Type Command
#define   ETH_PROTO_TCP_CMD_PM_VS            0x01
#define   ETH_PROTO_TCP_CMD_PM_READ_DATE     0x02
#define   ETH_PROTO_TCP_CMD_PM_READ_ID       0x03
#define   ETH_PROTO_TCP_CMD_PM_READ_REVISION 0x04
#define   ETH_PROTO_TCP_CMD_PM_READ_SERIAL   0x05
#define   ETH_PROTO_TCP_CMD_PM_VOLTAGE       0x06
#define   ETH_PROTO_TCP_CMD_PM_ENDISV        0x07
#define   ETH_PROTO_TCP_CMD_PM_READGW        0x08
#define   ETH_PROTO_TCP_CMD_PM_TURNOFFGW     0x18
#define   ETH_PROTO_TCP_CMD_PM_SET_DATE      0x09
#define   ETH_PROTO_TCP_CMD_PM_SET_ID        0x0A
#define   ETH_PROTO_TCP_CMD_PM_SET_REVISION  0x0B
#define   ETH_PROTO_TCP_CMD_PM_SET_SERIAL    0x0C

//SUB command
#define   ETH_PROTO_TCP_CMD_FAN_STEST       0x01//single fan
#define   ETH_PROTO_TCP_CMD_FAN_TACH        0x02
#define   ETH_PROTO_TCP_CMD_FAN_MTEST       0x03//all fans
#define   ETH_PROTO_TCP_CMD_FAN_RUN         0x04//run fan with selected dc
#define   ETH_PROTO_TCP_CMD_FAN_STOP        0x05//stop selected fan
#define   ETH_PROTO_TCP_CMD_FAN_ON          0x06//ON all fan controller output
#define   ETH_PROTO_TCP_CMD_FAN_OFF         0x07//OFF all fan controller output
#define   ETH_PROTO_TCP_CMD_FAN_LFD         0x08//Low Frequency Drive selection

#define   ETH_PROTO_TCP_CMD_LED_TEST        0x01
#define   ETH_PROTO_TCP_CMD_LED_GREEN       0x02
#define   ETH_PROTO_TCP_CMD_LED_YELLOW      0x03
#define   ETH_PROTO_TCP_CMD_LED_RED         0x04

#define   ETH_PROTO_TCP_CMD_CAN_TEST        0x01
#define   ETH_PROTO_TCP_CMD_CAN_RST         0x02
#define   ETH_PROTO_TCP_CMD_CAN_STA         0x03
#define   ETH_PROTO_TCP_CMD_CAN_MSG         0x04


#define   ETH_PROTO_TCP_CMD_NSH_R_TEST      0x01
#define   ETH_PROTO_TCP_CMD_NSH_R_STAT      0x02
#define   ETH_PROTO_TCP_CMD_NSH_R_RST       0x03

#define   ETH_PROTO_TCP_CMD_NSH_M_TEST      0x01
#define   ETH_PROTO_TCP_CMD_NSH_M_MOVE      0x02
#define   ETH_PROTO_TCP_CMD_NSH_M_READ      0x03
#define   ETH_PROTO_TCP_CMD_NSH_M_SNS       0x04
#define   ETH_PROTO_TCP_CMD_NSH_M_PRUN      0x05
#define   ETH_PROTO_TCP_CMD_NSH_M_STOP      0x06
#define   ETH_PROTO_TCP_CMD_NSH_M_SOFT_PRUN 0x07
#define   ETH_PROTO_TCP_CMD_NSH_M_SOFT_STOP 0x08

#define   ETH_PROTO_TCP_CMD_AUDIO_VOL       0x01
#define   ETH_PROTO_TCP_CMD_AUDIO_PLAY      0x02
#define   ETH_PROTO_TCP_CMD_AUDIO_READVOL   0x03
#define   ETH_PROTO_TCP_CMD_AUDIO_DAC       0x04
#define   ETH_PROTO_TCP_CMD_AUDIO_READDAC   0x05
#define   ETH_PROTO_TCP_CMD_AUDIO_GAIN      0x06
#define   ETH_PROTO_TCP_CMD_AUDIO_READGAIN  0x07
#define   ETH_PROTO_TCP_CMD_AUDIO_SETSOUND  0x08
#define   ETH_PROTO_TCP_CMD_AUDIO_STOP      0x09

#define   ETH_PROTO_TCP_CMD_CAMERA_VRS      0x01
#define   ETH_PROTO_TCP_CMD_CAMERA_TEST     0x02

#define   ETH_PROTO_TCP_CMD_EEPROM_TST      0x01

#define   ETH_PROTO_TCP_CMD_NTC_1           0x01
#define   ETH_PROTO_TCP_CMD_NTC_2           0x02
#define   ETH_PROTO_TCP_CMD_NTC_3           0x03
#define   ETH_PROTO_TCP_CMD_NTC_TEST        0x04

#define   ETH_PROTO_TCP_CMD_GPIO_SHD                0x01
#define   ETH_PROTO_TCP_CMD_GPIO_CONF               0x02

#define   ETH_PROTO_TCP_CMD_SERIAL_TEST             0x01

#define   ETH_PROTO_TCP_CMD_PWMDEVICE_TEST          0x01
#define   ETH_PROTO_TCP_CMD_PWMDEVICE_ENABLE        0x02
#define   ETH_PROTO_TCP_CMD_PWMDEVICE_DISABLE       0x03
#define   ETH_PROTO_TCP_CMD_PWMDEVICE_PERIOD        0x04
#define   ETH_PROTO_TCP_CMD_PWMDEVICE_DUTYCYCLE     0x05

#define  ETH_PROTO_TCP_CMD_TEC_START 0x01
#define  ETH_PROTO_TCP_CMD_TEC_STOP 0x02
#define  ETH_PROTO_TCP_CMD_TEC_PID_START 0x03
#define  ETH_PROTO_TCP_CMD_TEC_PID_STOP 0x04
#define  ETH_PROTO_TCP_CMD_TEC_PID_SETTEMP 0x05

#define   ETH_PROTO_TCP_CMD_REL_VRS                 0x01

//Info
#define ETH_PROTO_TCP_INFO_EEPROM_TEST              0x05


#endif // ETHPROTOINCLUDE_H
