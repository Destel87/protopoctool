#ifndef TEMPERATUREDIALOG_H
#define TEMPERATUREDIALOG_H

#include <QDialog>
#include <QTimer>
#include <QtWidgets/QLabel>

#include "tempThread.h"

namespace Ui {
class TemperatureDialog;
}

class TemperatureDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TemperatureDialog(QWidget *parent = nullptr);
    ~TemperatureDialog();

    void setActive(bool bVal);
    bool getActive();
    void setThread(tempThread* th);

    void setTemp(QString &str);

private slots:
    void on_pBStart_clicked();

    void on_pBStop_clicked();

private:
    void closeEvent (QCloseEvent *event);

    void addDataPlot(uint8_t numGraph, double val, QString str);

private:
    Ui::TemperatureDialog *ui;
    tempThread* _thread;

    bool _active;
};

#endif // TEMPERATUREDIALOG_H
