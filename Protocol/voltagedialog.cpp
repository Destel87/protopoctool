#include <QThread>
#include <QCloseEvent>
#include <QMessageBox>

#include "voltagedialog.h"
#include "ui_voltagedialog.h"

#define TITLE_DIALOG   "Voltage Displayer "

VoltageDialog::VoltageDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VoltageDialog)
{
    ui->setupUi(this);

    _active = false;

    QString strTitle;
    strTitle.append(TITLE_DIALOG);
    setWindowTitle(strTitle);

    ui->plotVoltage->yAxis->setTickLabels(false);
    connect(ui->plotVoltage->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->plotVoltage->yAxis2, SLOT(setRange(QCPRange)));
    connect(ui->plotVoltage->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->plotVoltage->xAxis2, SLOT(setRange(QCPRange)));
    ui->plotVoltage->yAxis2->setVisible(true);


    //GRAPH0
    ui->plotVoltage->addGraph(ui->plotVoltage->xAxis, ui->plotVoltage->axisRect()->axis(QCPAxis::atRight, 0));
    ui->plotVoltage->graph(0)->setPen(QPen(Qt::green));
    ui->plotVoltage->graph(0)->setName("v1");
    changePaletteLabel(ui->labelV1,Qt::green);

    //GRAPH1
    ui->plotVoltage->addGraph(ui->plotVoltage->xAxis, ui->plotVoltage->axisRect()->axis(QCPAxis::atRight, 0));
    ui->plotVoltage->graph(1)->setPen(QPen(Qt::red));
    changePaletteLabel(ui->labelV2,Qt::red);

    //GRAPH1
    ui->plotVoltage->addGraph(ui->plotVoltage->xAxis, ui->plotVoltage->axisRect()->axis(QCPAxis::atRight, 0));
    ui->plotVoltage->graph(2)->setPen(QPen(Qt::blue));
    changePaletteLabel(ui->labelV3,Qt::blue);

    //GRAPH3
    ui->plotVoltage->addGraph(ui->plotVoltage->xAxis, ui->plotVoltage->axisRect()->axis(QCPAxis::atRight, 0));
    ui->plotVoltage->graph(3)->setPen(QPen(Qt::gray));
    changePaletteLabel(ui->labelV4,Qt::gray);

    //GRAPH4
    QColor darkkhaki = QColor(189,183,107);
    ui->plotVoltage->addGraph(ui->plotVoltage->xAxis, ui->plotVoltage->axisRect()->axis(QCPAxis::atRight, 0));
    ui->plotVoltage->graph(4)->setPen(QPen(darkkhaki));//DarkKhaki
    changePaletteLabel(ui->labelV5, darkkhaki);

    //GRAPH5
    QColor orange = QColor(255,165,0);
    ui->plotVoltage->addGraph(ui->plotVoltage->xAxis, ui->plotVoltage->axisRect()->axis(QCPAxis::atRight, 0));
    ui->plotVoltage->graph(5)->setPen(QPen(orange));//orange
    changePaletteLabel(ui->labelV6, orange);

    //GRAPH6
    QColor hotpink = QColor(255,105,180);
    ui->plotVoltage->addGraph(ui->plotVoltage->xAxis, ui->plotVoltage->axisRect()->axis(QCPAxis::atRight, 0));
    ui->plotVoltage->graph(6)->setPen(QPen(hotpink));//hot pink
    changePaletteLabel(ui->labelV7, hotpink);

    //GRAPH7
    QColor saddlebrown = QColor(139,69,19);
    ui->plotVoltage->addGraph(ui->plotVoltage->xAxis, ui->plotVoltage->axisRect()->axis(QCPAxis::atRight, 0));
    ui->plotVoltage->graph(7)->setPen(QPen(saddlebrown));//SaddleBrown
    changePaletteLabel(ui->labelV8,saddlebrown);

    ui->plotVoltage->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

    //TODO To remove! Only for debugging!
//    connect(&mDataTimer, SIGNAL(timeout()), this, SLOT(timerSlot()));

    ui->plotVoltage->yAxis->setRange(0,5);
    ui->plotVoltage->yAxis->setRange(0,25);

    testvalue = 0;
    testvalue2 = 0;
    testvalue3 = 0;

}

VoltageDialog::~VoltageDialog()
{
    delete ui;
}

void VoltageDialog::changePaletteLabel(QLabel *label, QColor color)
{
    QPalette palette;

    palette = label->palette();
    palette.setColor(label->backgroundRole(), color);
    palette.setColor(label->foregroundRole(), color);
    label->setPalette(palette);
}

void VoltageDialog::closeEvent (QCloseEvent *event)
{
    if ((_thread != nullptr) && (_thread->isRunning()))
    {
        _thread->stopThread();
    }

//    ui->plotVoltage->clearItems();
//    ui->plotVoltage->clearGraphs();
    setActive(false);
    event->accept();
}


void VoltageDialog::setActive(bool bVal)
{
    _active = bVal;
}

bool VoltageDialog::getActive()
{
    return _active;
}

void VoltageDialog::setThread(VoltageThread* thread)
{
    _thread = thread;

}

void VoltageDialog::on_pushButtonStart_clicked()
{
    _thread->start();

    //TODO to remove
//    mDataTimer.start(1000);
}

void VoltageDialog::on_pushButtonStop_clicked()
{
    _thread->stopThread();

    //TODO to remove
//    mDataTimer.stop();
}

void VoltageDialog::setV1(QString &str)
{
    ui->lineEditV1->setText(str);
    QString cpstr;
    cpstr = str;
    str.remove(4,1);
    double volt = str.toDouble();
    addDataPlot(0,volt,cpstr);
}

void VoltageDialog::setV2(QString &str)
{
    ui->lineEditV2->setText(str);
    QString cpstr;
    cpstr = str;
    str.remove(4,1);
    double volt = str.toDouble();
    addDataPlot(1,volt,cpstr);
}

void VoltageDialog::setV3(QString &str)
{
    ui->lineEditV3->setText(str);
    QString cpstr;
    cpstr = str;
    str.remove(4,1);
    double volt = str.toDouble();
    addDataPlot(2,volt,cpstr);
}

void VoltageDialog::setV4(QString &str)
{
    ui->lineEditV4->setText(str);
    QString cpstr;
    cpstr = str;
    str.remove(4,1);
    double volt = str.toDouble();
    addDataPlot(3,volt,cpstr);
}

void VoltageDialog::setV5(QString &str)
{
    ui->lineEditV5->setText(str);
    QString cpstr;
    cpstr = str;
    str.remove(4,1);
    double volt = str.toDouble();
    addDataPlot(4,volt,cpstr);
}

void VoltageDialog::setV6(QString &str)
{
    ui->lineEditV6->setText(str);
    QString cpstr;
    cpstr = str;
    str.remove(4,1);
    double volt = str.toDouble();
    addDataPlot(5,volt,cpstr);
}

void VoltageDialog::setV7(QString &str)
{
    ui->lineEditV7->setText(str);
    QString cpstr;
    cpstr = str;
    str.remove(4,1);
    double volt = str.toDouble();
    addDataPlot(6,volt,cpstr);
}

void VoltageDialog::setV8(QString &str)
{
    ui->lineEditV8->setText(str);
    QString cpstr;
    cpstr = str;
    str.remove(4,1);
    double volt = str.toDouble();
    addDataPlot(7,volt,cpstr);
}

void VoltageDialog::timerSlot()
{
    testvalue++;
    ui->plotVoltage->graph(0)->addData(ui->plotVoltage->graph(0)->dataCount(), testvalue);
    if (testvalue >= 100)
    {
        testvalue = 0;
    }

    testvalue2++;
    ui->plotVoltage->graph(1)->addData(ui->plotVoltage->graph(0)->dataCount(), testvalue2);
    if (testvalue2>=10)
    {
        testvalue2 = 0;
    }

    testvalue3 = testvalue3 + 2;
    ui->plotVoltage->graph(2)->addData(ui->plotVoltage->graph(0)->dataCount(), testvalue3);
    if (testvalue3>=200)
    {
        testvalue3 = 0;
    }

    ui->plotVoltage->xAxis->rescale();
    ui->plotVoltage->graph(0)->rescaleValueAxis(false, true);
    //
    ui->plotVoltage->graph(1)->rescaleValueAxis(false,true);
    //
    ui->plotVoltage->graph(2)->rescaleValueAxis(false,true);
//    mPlot->xAxis->setRange(mPlot->xAxis->range().upper, 100, Qt::AlignRight);
//    mPlot->yAxis->rescale();
//    mPlot->yAxis->setRange(100, mPlot->yAxis->range().upper, Qt::AlignTop);



//    mPlot->rescaleAxes();
    ui->plotVoltage->graph(0)->rescaleAxes();
//    mPlot->graph(1)->rescaleAxes(true);
    ui->plotVoltage->replot();
}

void VoltageDialog::addDataPlot(uint8_t numGraph, double val, QString str)
{
    // add the text label at the top:
    QCPItemText *textLabel = new QCPItemText(ui->plotVoltage);
    textLabel->setPositionAlignment(Qt::AlignTop|Qt::AlignLeading);
    textLabel->position->setType(QCPItemPosition::ptAxisRectRatio);
    textLabel->position->setCoords(0.5, 0); // place position at center/top of axis rect
    textLabel->setText(str);
    textLabel->setFont(QFont(font().family(), 16)); // make font a bit larger
    textLabel->setPen(QPen(Qt::black)); // show black border around text

    // add the arrow:
    QCPItemLine *arrow = new QCPItemLine(ui->plotVoltage);
    arrow->start->setParentAnchor(textLabel->bottom);
    arrow->end->setCoords(ui->plotVoltage->graph(numGraph)->dataCount(), val); // point to (4, 1.6) in x-y-plot coordinates
    arrow->setHead(QCPLineEnding::esSpikeArrow);

    // add data at the custom plot
    ui->plotVoltage->graph(numGraph)->addData(ui->plotVoltage->graph(numGraph)->dataCount(), val);
    ui->plotVoltage->xAxis->rescale();

    ui->plotVoltage->replot();

    ui->plotVoltage->removeItem(textLabel);
    ui->plotVoltage->removeItem(arrow);
}
