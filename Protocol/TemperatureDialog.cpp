#include <QThread>
#include <QCloseEvent>
#include <QMessageBox>

#include "TemperatureDialog.h"
#include "ui_temperaturedialog.h"

#define TITLE_DIALOG   "Temperature Tracker "

TemperatureDialog::TemperatureDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TemperatureDialog)
{
    ui->setupUi(this);

    _active = false;

    QString strTitle;
    strTitle.append(TITLE_DIALOG);
    setWindowTitle(strTitle);

    ui->plotTemp->yAxis->setTickLabels(false);
    connect(ui->plotTemp->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->plotTemp->yAxis2, SLOT(setRange(QCPRange)));
    connect(ui->plotTemp->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->plotTemp->xAxis2, SLOT(setRange(QCPRange)));
    ui->plotTemp->yAxis2->setVisible(true);

    //GRAPH0
    ui->plotTemp->addGraph(ui->plotTemp->xAxis, ui->plotTemp->axisRect()->axis(QCPAxis::atRight, 0));
    ui->plotTemp->graph(0)->setPen(QPen(Qt::green));
    ui->plotTemp->graph(0)->setName("Temperature");

    ui->plotTemp->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

    ui->plotTemp->yAxis->setRange(0,5);
    ui->plotTemp->yAxis->setRange(25,45);
}

TemperatureDialog::~TemperatureDialog()
{
    delete ui;
}

void TemperatureDialog::setThread(tempThread* thread)
{
    _thread = thread;
}

void TemperatureDialog::setActive(bool bVal)
{
    _active = bVal;
}

bool TemperatureDialog::getActive()
{
    return _active;
}

void TemperatureDialog::setTemp(QString &str)
{
//    ui->lineEditV1->setText(str);
    QString cpstr;
    cpstr = str;
    str.remove(4,2);
    double temp = str.toDouble();
    addDataPlot(0, temp, cpstr);
}

void TemperatureDialog::addDataPlot(uint8_t numGraph, double val, QString str)
{
    // add the text label at the top:
    QCPItemText *textLabel = new QCPItemText(ui->plotTemp);
    textLabel->setPositionAlignment(Qt::AlignTop|Qt::AlignLeading);
    textLabel->position->setType(QCPItemPosition::ptAxisRectRatio);
    textLabel->position->setCoords(0.5, 0); // place position at center/top of axis rect
    textLabel->setText(str);
    textLabel->setFont(QFont(font().family(), 16)); // make font a bit larger
    textLabel->setPen(QPen(Qt::black)); // show black border around text

    // add the arrow:
    QCPItemLine *arrow = new QCPItemLine(ui->plotTemp);
    arrow->start->setParentAnchor(textLabel->bottom);
    arrow->end->setCoords(ui->plotTemp->graph(numGraph)->dataCount(), val); // point to (4, 1.6) in x-y-plot coordinates
    arrow->setHead(QCPLineEnding::esSpikeArrow);

    // add data at the custom plot
    ui->plotTemp->graph(numGraph)->addData(ui->plotTemp->graph(numGraph)->dataCount(), val);
    ui->plotTemp->xAxis->rescale();
    ui->plotTemp->replot();
    ui->plotTemp->removeItem(textLabel);
    ui->plotTemp->removeItem(arrow);
}

void TemperatureDialog::closeEvent (QCloseEvent *event)
{
    if ((_thread != nullptr) && (_thread->isRunning()))
    {
        _thread->stopThread();
    }

    setActive(false);
    event->accept();
}

void TemperatureDialog::on_pBStart_clicked()
{
    _thread->start();
}


void TemperatureDialog::on_pBStop_clicked()
{
    _thread->stopThread();
}
