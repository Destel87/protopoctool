#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "EthTcpSocket.h"
#include "password.h"
#include "Console/consolewindow.h"
#include "textresultwindow.h"
#include "voltagedialog.h"
#include "TemperatureDialog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

typedef enum
{
    ETH_PROTO_STX = 0,
    ETH_PROTO_LEN = 1,
    ETH_PROTO_CMD = 2,
    ETH_PROTO_DATA = 3,
    ETH_PROTO_CRC = 4,
    ETH_PROTO_ETX = 5
}EthProtoState;

#define MSG_TX      0x00
#define MSG_RX      0x01

#define GEOM_HEIGHT 670
//#define GEOM_WIDTH  1130
#define GEOM_WIDTH  850
#define GEOM_HEIGHT_ST GEOM_HEIGHT+120

#define BACKGND_GREEN "background-color: green"
#define BACKGND_RED "background-color: red"
#define BACKGND_DEFAULT "background-color: gray"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void enablePeripheralsBox(bool enable);
    void clearTextObjects(void);
    void buildMsg(quint16 usLen, quint8 ubCmd, QByteArray arrayData);
    void parseMsg(QByteArray arrayMsg);
    void managePMAns(QByteArray data);
    void manageFANAns(QByteArray data);
    void manageLEDAns(QByteArray data);
    void manageCANAns(QByteArray data);
    void manageNSHRAns(QByteArray data);
    void manageNSHMAns(QByteArray data);
    void manageAudioAns(QByteArray data);
    void manageCameraAns(QByteArray data);
    void manageEEpromAns(QByteArray data);
    void manageNTCAns(QByteArray data);
    void manageGPIOAns(QByteArray data);
    void manageHeaterAns(QByteArray data);
    void manageTecAns(QByteArray data);
    void manageSerialAns(QByteArray data);
    void manageReleaseVersion(QByteArray data);
    void printMessage(quint8 ubTxRx, QByteArray data);
    void waitForExecution(bool bVal);

    void closeEvent (QCloseEvent *event);
    void initActions( void );

private slots:
    void on_buttonConnect_clicked();
    void on_buttonDisconnect_clicked();
    void readDataClient();
    void on_pushButtonPMReadVoltage_clicked();
    void on_pushButtonPMRelease_clicked();
    void on_pushButtonPMEnable_clicked();
    void on_pushButtonTestSingleFan_clicked();
    void on_pushButtonReadTachimeter_clicked();
    void on_pushButtonTestAllFan_clicked();
    void on_pushButtonLEDTest_clicked();
    void on_pushButtonCANTest_clicked();
    void on_pushButtonCANReset_clicked();
    void on_pushButtonCANStatus_clicked();
    void on_pushButtonNSHRTest_clicked();
    void on_pushButtonNSHRStat_clicked();
    void on_pushButtonNSHRReset_clicked();
    void on_pushButtonNSHMTest_clicked();
    void on_pushButtonNSHMMove_clicked();
    void on_pushButtonNSHMReadSteps_clicked();
    void on_pushButtonNSHMReadSensor_clicked();
    void on_pushButtonAudioPlay_clicked();
    void on_pushButtonCameraVersion_clicked();
    void on_pushButtonLEDGreenTest_clicked();
    void on_pushButtonLEDYellowTest_clicked();
    void on_pushButtonLEDRedTest_clicked();
    void on_pushButtonLEDGreenTest_2_clicked();
    void on_pushButtonLEDYellowTest_2_clicked();
    void on_pushButtonLEDRedTest_2_clicked();
    void on_pushButtonPMDisable_clicked();
    void on_verticalSliderVolumeAudio_sliderReleased();
    void on_pushButtonEEpromTest_clicked();
    void on_pushButtonNTC1Test_clicked();
    void on_pushButtonNTC2Test_clicked();
    void on_pushButtonNTC3Test_clicked();
    void on_pushButtonPMReadGWStatus_clicked();
    void on_pushButtonShutdown_clicked();
    void on_pushButtonPMDate_clicked();
    void on_pushButtonPMIDConf_clicked();
    void on_pushButtonPMRevision_clicked();
    void on_pushButtonPMSerial_clicked();
    void on_pushButtonPMSetDate_clicked();
    void on_pushButtonPMSetIDConf_clicked();
    void on_pushButtonPMSetSerial_clicked();
    void on_pushButtonPMSetRevision_clicked();
    void on_pushButtonGWShutdown_clicked();
    void on_pushButtonFANStart_clicked();
    void on_pushButtonFANStop_clicked();
    void on_pushButtonNTCTest_clicked();
    void on_pushButtonCameraTest_clicked();
    void on_pushButtonHeaterEnable_clicked();
    void on_pushButtonHeaterTest_clicked();
    void on_pushButtonHeaterDisable_clicked();
    void on_pushButtonSerialTest_clicked();
    void on_verticalSliderVolumeGain_sliderReleased();
    void on_verticalSliderVolumeDAC_sliderReleased();
    void readVolume();
    void readDAC();
    void readDriverGain();
    void on_pushButtonConsole_clicked();
    void on_pushButtonTextResult_clicked();
    void on_actionVoltage(void);
    void on_actionTemperature(void);
    void about(void);
    void on_pushButtonConfig_clicked();
    void on_pushButtonReleaseVrs_clicked();

    void on_pushButtonCANSend_clicked();

    void on_pushButtonMotorRun_clicked();

    void on_pushButtonMotorStop_clicked();

    void on_pushButtonPwmDeviceSetPeriod_clicked();

    void on_pushButtonPwmDeviceDutycycle_clicked();

    void on_pushButtonEmON_clicked();

    void on_pushButtonEmOFF_clicked();

    void on_pB_TECDriverOn_clicked();

    void on_pB_TECDriverOff_clicked();

    void on_pBMotorSoftStart_clicked();

    void on_pBMotorSoftStop_clicked();

    void on_pB_StartPid_clicked();

    void on_pB_StopPid_clicked();

    void on_pB_FANSetPeriod_clicked();

    void on_pBStopAudio_clicked();

    void on_cBSelection_currentIndexChanged(int index);

    void on_pB_SetTemp_clicked();

public slots:
    void on_requestVoltagePM(int val);
    void on_requestTemperature(int val);

private:
    Ui::MainWindow *ui;
    EthTcpSocket    socketTcp;
    EthProtoState   _ethState;

    QByteArray      _protoEthArray;
    QByteArray      _protoEthReceiveArray;
    Password        _pwd;
    ConsoleWindow   _console;
    TextResultWindow _textresult;
    VoltageDialog    _voltageD;
    VoltageThread*   _mVoltageThread;
    TemperatureDialog _tempD;
    tempThread*       _tempThread;
    int              _lVoltageRequested;
    bool             _TECDriverEnabled;
    bool             _PIDEnabled;
};
#endif // MAINWINDOW_H
