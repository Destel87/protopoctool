#ifndef VOLTAGEDIALOG_H
#define VOLTAGEDIALOG_H

#include <QDialog>
#include <QTimer>
#include <QtWidgets/QLabel>
#include "voltagethread.h"


namespace Ui {
class VoltageDialog;
}

class VoltageDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VoltageDialog(QWidget *parent = nullptr);
    ~VoltageDialog();

    void setActive(bool bVal);
    bool getActive();
    void setThread(VoltageThread *thread);
    void setV1(QString &str);
    void setV2(QString &str);
    void setV3(QString &str);
    void setV4(QString &str);
    void setV5(QString &str);
    void setV6(QString &str);
    void setV7(QString &str);
    void setV8(QString &str);

private:
    void closeEvent (QCloseEvent *event);

    void addDataPlot(uint8_t numGraph, double val, QString str);

    void changePaletteLabel(QLabel * label, QColor color);

private slots:
    void on_pushButtonStart_clicked();

    void on_pushButtonStop_clicked();

    void timerSlot();

private:
    Ui::VoltageDialog *ui;

    QTimer mDataTimer;

    bool _active;
    VoltageThread *_thread;

    int testvalue;
    int testvalue2;
    int testvalue3;
};

#endif // VOLTAGEDIALOG_H
