#include "voltagethread.h"
#include <QtCore>

VoltageThread::VoltageThread()
{
    _StopThread = false;
    _voltage = 1;
}

void VoltageThread::run()
{
    while(this->isRunning())
    {
        QMutex mutex;
        mutex.lock();
        if(this->_StopThread)
        {
            _StopThread = false;
            break;
        }
        mutex.unlock();

        emit requestVoltage(_voltage);

        _voltage++;
        if (_voltage >= 9)
        {
         _voltage = 1;
        }

        this->msleep(UPDATE_V_MS);
    }
}


void VoltageThread::stopThread()
{
    _StopThread = true;
}
