#include "tempThread.h"
#include <QtCore>

tempThread::tempThread()
{
    _StopThread = false;
    _ntc = 0;
}

void tempThread::run()
{
    while(this->isRunning())
    {
        QMutex mutex;
        mutex.lock();
        if (this->_StopThread)
        {
            _StopThread = false;
            break;
        }
        mutex.unlock();

        emit requestTemperature(_ntc);


        this->msleep(UPDATE_T_MS);
    }
}


void tempThread::stopThread()
{
    _StopThread = true;
}

void tempThread::setNTC(int val)
{
    _ntc = val;
}
