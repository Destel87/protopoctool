#ifndef TEMPTHREAD_H
#define TEMPTHREAD_H

#include <QObject>
#include <QWidget>
#include <QThread>

#define UPDATE_T_MS 1000

class tempThread : public QThread
{
    Q_OBJECT
public:
    tempThread();
    void run();
    void stopThread(void);
    void setNTC(int val);

signals:
    void requestTemperature(int);

public slots:

public:
    bool _StopThread;
    int _ntc;
};

#endif // TEMPTHREAD_H
