#include "textresultwindow.h"
#include "ui_textresultwindow.h"

TextResultWindow::TextResultWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TextResultWindow)
{
    ui->setupUi(this);
}

TextResultWindow::~TextResultWindow()
{
    delete ui;
}

void TextResultWindow::printText(QString str)
{
    ui->textResultEdit->append(str);
}

void TextResultWindow::setActive(bool bVal)
{
    _active = bVal;
}

bool TextResultWindow::getActive()
{
    return _active;
}


