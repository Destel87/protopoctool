#ifndef TEXTRESULTWINDOW_H
#define TEXTRESULTWINDOW_H

#include <QMainWindow>

namespace Ui {
class TextResultWindow;
}

class TextResultWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit TextResultWindow(QWidget *parent = nullptr);
    ~TextResultWindow();

    void printText(QString str);

    void setActive(bool bVal);
    bool getActive();

private:
    Ui::TextResultWindow *ui;

    bool _active;
};

#endif // TEXTRESULTWINDOW_H
