#include <QCloseEvent>
#include <QMessageBox>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "version.h"
#include "EthProtoInclude.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _protoEthArray.clear();
    _protoEthReceiveArray.clear();
    _ethState = ETH_PROTO_STX;

    ui->lineEditServerIP->setText("192.168.0.2");
    ui->lineEditPort->setText("2105");

    ui->lineEditPwmDeviceDutyCycle->setText("100");
    ui->lineEditPwmDevicePeriod->setText("100000");
    ui->lineEditNSHMMotorSpeed->setText("200");
    ui->lineEditDCValue->setText("100");
    ui->lE_SetTemp->setText("37.5");
//    ui->labelLogo->setPixmap(QPixmap(":logo.png"));

    ui->actionQuit->setEnabled(true);
    ui->actionTerminal->setEnabled(true);
    ui->actionAbout->setEnabled(true);
    ui->actionText_View->setEnabled(true);
    ui->actionVoltage->setEnabled(true);
    initActions();

    setGeometry(0,50,GEOM_WIDTH,GEOM_HEIGHT);
    resize(GEOM_WIDTH,GEOM_HEIGHT);
//    setFixedSize(GEOM_WIDTH,GEOM_HEIGHT);

    QString strTitle;
    strTitle.append(TITLE);
    strTitle.append(VERSION);
    setWindowTitle(strTitle);

    /*************************************************************/
    // Voltage Thread setting: it creates the thread
    _mVoltageThread = new VoltageThread();
    _voltageD.setThread(_mVoltageThread);
    connect(_mVoltageThread,  SIGNAL(requestVoltage(int)), this, SLOT(on_requestVoltagePM(int)));

    /*************************************************************/
    // Temperature Thread
    _tempThread = new tempThread();
    _tempD.setThread(_tempThread);
    connect(_tempThread, SIGNAL(requestTemperature(int)), this, SLOT(on_requestTemperature(int)));

    _TECDriverEnabled = false;
    _PIDEnabled = false;

}

MainWindow::~MainWindow()
{
    delete _mVoltageThread;
    delete _tempThread;

    _console.close();
    _textresult.close();
//    _voltageD.close();

    delete ui;
}

void MainWindow::initActions()
{
    //Menù Bar action
    connect(ui->actionTerminal, &QAction::triggered, this, &MainWindow::on_pushButtonConsole_clicked);
    connect(ui->actionQuit, &QAction::triggered, this, &MainWindow::close);
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::about);
    connect(ui->actionText_View, &QAction::triggered, this, &MainWindow::on_pushButtonTextResult_clicked);
    connect(ui->actionVoltage, &QAction::triggered, this, &MainWindow::on_actionVoltage);
    connect(ui->actionTemperature,  &QAction::triggered, this, &MainWindow::on_actionTemperature);
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About Proto POC Tool"),
                       tr("The <b>Proto POC Tool</b> provides the commands "
                          "to handle all the peripheral board functionalities."));
}

void MainWindow::clearTextObjects()
{

    ui->labelReleaseVersion->clear();

    /* Power Manager */
    ui->lineEditPMDate->clear();
    ui->lineEditPMRelease->clear();
    ui->lineEditPMIDConf->clear();
    ui->lineEditPMSerial->clear();
    ui->lineEditPMVoltage->clear();
    ui->lineEditPMRevision->clear();
//    ui->labelGWStatus->clear();

    /* FAN */
    ui->lineEditTachValue->clear();
    ui->lineEditDCValue->clear();
    ui->labelFANAllTestResult->clear();
    ui->labelFANSingleTestResult->clear();

    /* NTC */
    ui->lineEditNTC1Value->clear();
    ui->lineEditNTC2Value->clear();
    ui->lineEditNTC3Value->clear();
    ui->labelNTCTestResult->clear();

    /* HEATER */
    ui->labelHeaterTestResult->clear();

    /* SERIAL */
//    ui->labelSerialTestResult->clear();

    /* CAN */
//    ui->lineEditCANStatus->clear();
//    ui->labelCANTestResult->clear();
//    ui->lineEditCANMsg->clear();

    /* LED DRIVER */
    ui->labelLedTestResult->clear();
    ui->labelLedRedTestResult->clear();
    ui->labelLedGreenTestResult->clear();
    ui->labelLedYellowTestResult->clear();

    /* CAMERA */
    ui->labelCameraTest->clear();
    ui->lineEditCameraVersion->clear();

    /* EEPROM */
    ui->labelEEpromTestResult->clear();

    /* NSH MOTOR*/
//    ui->labelNSHReader->clear();
//    ui->labelNSHRTestResult->clear();
//    ui->lineEditNSHRStatus->clear();

    ui->labelNSHMSensor->clear();
    ui->lineEditNSHMMoveSteps->clear();
    ui->lineEditNSHMReadSteps->clear();

    /* AUDIO */
    ui->labelAudioDAC->clear();
    ui->labelAudioVolume->clear();
    ui->labelAudioDriverGain->clear();
}

void MainWindow::enablePeripheralsBox(bool enable)
{
    ui->groupBoxPM->setEnabled(enable);
//    ui->groupBoxCAN->setEnabled(enable);
    ui->groupBoxLED->setEnabled(enable);
    ui->groupBoxNSH->setEnabled(enable);
    ui->groupBoxFAN->setEnabled(enable);
    ui->groupBoxAudio->setEnabled(enable);
    ui->groupBoxEEprom->setEnabled(enable);
    ui->groupBoxCamera->setEnabled(enable);
    ui->groupBoxNTC->setEnabled(enable);
    ui->pushButtonShutdown->setEnabled(enable);
    ui->groupBoxHeater->setEnabled(enable);
//    ui->groupBoxSerial->setEnabled(enable);
    ui->groupBoxHWConfig->setEnabled(enable);
    ui->pushButtonReleaseVrs->setEnabled(enable);
    ui->groupBoxTECDriver->setEnabled(enable);

//    clearTextObjects();

}

void MainWindow::printMessage(quint8 ubTxRx, QByteArray data)
{
    if (_textresult.getActive() == true)
    {
        if (ubTxRx == MSG_TX)
        {
            _textresult.printText("TX-->");
        }
        else if ( ubTxRx == MSG_RX)
        {
            _textresult.printText("RX-->");
        }
        else
        {

        }
        _textresult.printText(data.toHex());
    }
}

void MainWindow::waitForExecution(bool bVal)
{
    if (bVal == true)
    {
        this->setCursor(Qt::WaitCursor);
        enablePeripheralsBox(false);
    }
    else
    {
        this->setCursor(Qt::ArrowCursor);
        enablePeripheralsBox(true);
    }
}

/* ************************************************************
 *
 * ETHERNET
 *
 * ************************************************************/
void MainWindow::on_buttonConnect_clicked()
{
    QString ipAddress;
    QString portNumber;

    ipAddress = ui->lineEditServerIP->text();
    portNumber = ui->lineEditPort->text();

    qint16 iportNumber;
    iportNumber = portNumber.toUShort();
    if ((portNumber == "") || (ipAddress == ""))
    {
        qDebug()  << "values not present...";
    }
    else
    {
        socketTcp.doConnect(ipAddress, iportNumber);
        connect(&socketTcp, SIGNAL(dataReady()), this, SLOT(readDataClient()));

        readVolume();
    }
}

void MainWindow::on_buttonDisconnect_clicked()
{
   socketTcp.closeConnection();

   enablePeripheralsBox(false);

   ui->pushButtonShutdown->setStyleSheet(QStringLiteral(""));

   ui->buttonConnect->setEnabled(true);
   ui->buttonDisconnect->setEnabled(false);
}

/* ************************************************************
 *
 * POWER MANAGER
 *
 * ************************************************************/
void MainWindow::on_pushButtonPMReadVoltage_clicked()
{
    quint8  ubInfo = 0;
    QByteArray   array;

    ubInfo = ui->comboBoxPMVoltage->currentIndex()+1;
    array.push_back(ETH_PROTO_TCP_CMD_PM_VOLTAGE);
    array.push_back(ubInfo);

    buildMsg(0x03, ETH_PROTO_TCP_CMD_PM, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_requestVoltagePM(int val)
{
//    quint8  ubInfo = 0;
    QByteArray   array;

    _lVoltageRequested = val;

    array.push_back(ETH_PROTO_TCP_CMD_PM_VOLTAGE);
    array.push_back(_lVoltageRequested);

    buildMsg(0x03, ETH_PROTO_TCP_CMD_PM, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonPMRelease_clicked()
{
//    quint8  ubInfo = 0;
    quint8  ubType = 0;
    QByteArray   array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_PM_VS;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_PM, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonPMEnable_clicked()
{
    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_PM_ENDISV;
    array.push_back(ubType);

    ubInfo = ui->comboBoxPMEnableDisable->currentIndex();
    array.push_back(ubInfo);

    ubInfo = 0x01;
    array.push_back(ubInfo);

    ubLen = 4;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_PM, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonPMDisable_clicked()
{
    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_PM_ENDISV;
    array.push_back(ubType);

    ubInfo = ui->comboBoxPMEnableDisable->currentIndex();
    array.push_back(ubInfo);

    ubInfo = 0x00;
    array.push_back(ubInfo);

    ubLen = 4;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_PM, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonPMReadGWStatus_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_PM_READGW;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_PM, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonShutdown_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_GPIO_SHD;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_GPIO, array);
    socketTcp.sendData(_protoEthArray);

    on_buttonDisconnect_clicked();

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonConfig_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_GPIO_CONF;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_GPIO, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonPMDate_clicked()
{
        quint8  ubType = 0;
        QByteArray   array;
        quint8      ubLen;

        //DATA
        ubType = ETH_PROTO_TCP_CMD_PM_READ_DATE;
        array.push_back(ubType);

        ubLen = 2;
        buildMsg(ubLen, ETH_PROTO_TCP_CMD_PM, array);

        socketTcp.sendData(_protoEthArray);

        printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonPMIDConf_clicked() {
    quint8  ubType = 0;
    QByteArray   array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_PM_READ_ID;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_PM, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonPMRevision_clicked()
{
    quint8  ubType = 0;
    QByteArray   array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_PM_READ_REVISION;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_PM, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonPMSerial_clicked()
{
    quint8  ubType = 0;
    QByteArray   array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_PM_READ_SERIAL;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_PM, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonPMSetDate_clicked()
{
    quint8  ubType = 0;
    QByteArray   array;
    quint8      ubLen;

    _pwd.exec();
    //Pending on the dialog box
    if (_pwd.getPwd() == true)
    {
        //DATA
        ubType = ETH_PROTO_TCP_CMD_PM_SET_DATE;
        array.push_back(ubType);
        QString date = ui->lineEditPMDate->text();
        array.push_back(date.toUtf8());
        ubLen = 8;
        buildMsg(ubLen, ETH_PROTO_TCP_CMD_PM, array);

        socketTcp.sendData(_protoEthArray);
        printMessage(MSG_TX, _protoEthArray);
    }
}

void MainWindow::on_pushButtonPMSetIDConf_clicked()
{
    quint8  ubType = 0;
    QByteArray   array;
    quint8      ubLen;

    _pwd.exec();
    //Pending on the dialog box
    if (_pwd.getPwd() == true)
    {
        //DATA
        ubType = ETH_PROTO_TCP_CMD_PM_SET_ID;
        array.push_back(ubType);
        QString date = ui->lineEditPMIDConf->text();
        array.push_back(date.toUtf8());
        ubLen = 14;
        buildMsg(ubLen, ETH_PROTO_TCP_CMD_PM, array);

        socketTcp.sendData(_protoEthArray);

        printMessage(MSG_TX, _protoEthArray);
    }
}

void MainWindow::on_pushButtonPMSetSerial_clicked()
{
    quint8  ubType = 0;
    QByteArray   array;
    quint8      ubLen;

    _pwd.exec();
    //Pending on the dialog box
    if (_pwd.getPwd() == true)
    {
        //DATA
        ubType = ETH_PROTO_TCP_CMD_PM_SET_SERIAL;
        array.push_back(ubType);
        QString date = ui->lineEditPMSerial->text();
        array.push_back(date.toUtf8());
        ubLen = 6;
        buildMsg(ubLen, ETH_PROTO_TCP_CMD_PM, array);

        socketTcp.sendData(_protoEthArray);

        printMessage(MSG_TX, _protoEthArray);
    }
}

void MainWindow::on_pushButtonPMSetRevision_clicked()
{
    quint8  ubType = 0;
    QByteArray   array;
    quint8      ubLen;

    _pwd.exec();
    //Pending on the dialog box
    if (_pwd.getPwd() == true)
    {
        //DATA
        ubType = ETH_PROTO_TCP_CMD_PM_SET_REVISION;
        array.push_back(ubType);
        QString date = ui->lineEditPMRevision->text();
        array.push_back(date.toUtf8());
        ubLen = 10;
        buildMsg(ubLen, ETH_PROTO_TCP_CMD_PM, array);

        socketTcp.sendData(_protoEthArray);
        printMessage(MSG_TX, _protoEthArray);
    }
}

void MainWindow::on_pushButtonGWShutdown_clicked()
{
    quint8  ubType = 0;
    QByteArray   array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_PM_TURNOFFGW;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_PM, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

/* ************************************************************
 *
 * FAN
 *
 * ************************************************************/
void MainWindow::on_pushButtonTestSingleFan_clicked()
{
    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_FAN_STEST;
    array.push_back(ubType);
    ubInfo = ui->comboBoxFan->currentIndex();
    array.push_back(ubInfo);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_FAN, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
    waitForExecution(true);
}

void MainWindow::on_pushButtonReadTachimeter_clicked()
{
    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_FAN_TACH;
    array.push_back(ubType);
    ubInfo = ui->comboBoxFan->currentIndex();
    array.push_back(ubInfo);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_FAN, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonTestAllFan_clicked()
{
//    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_FAN_MTEST;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_FAN, array);
    socketTcp.sendData(_protoEthArray);

    waitForExecution(true);
    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonLEDTest_clicked()
{
//    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_LED_TEST;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_LED, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonFANStart_clicked()
{
    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray   array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_FAN_RUN;
    array.push_back(ubType);

    ubInfo = ui->comboBoxFan->currentIndex();
    array.push_back(ubInfo);

    QString sInfo = ui->lineEditDCValue->text();
    if ((sInfo.toInt() >= 0) && (sInfo.toInt() <= 100))
    {
        array.push_back(sInfo.toInt());
    }
    else
    {
        char def = 0;
        array.push_back(def);
    }


    ubLen = 4;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_FAN, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
    waitForExecution(true);
}

void MainWindow::on_pushButtonFANStop_clicked()
{
    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray   array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_FAN_STOP;
    array.push_back(ubType);

    ubInfo = ui->comboBoxFan->currentIndex();
    array.push_back(ubInfo);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_FAN, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
    waitForExecution(true);
}

void MainWindow::on_pushButtonEmON_clicked()
{
//    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray   array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_FAN_ON;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_FAN, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonEmOFF_clicked()
{
    quint8      ubType = 0;
    QByteArray   array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_FAN_OFF;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_FAN, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pB_FANSetPeriod_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;
    quint8      ubInfo;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_FAN_LFD;
    array.push_back(ubType);

    ubInfo = ui->cB_FANPeriod->currentIndex();
    array.push_back(ubInfo);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_FAN, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}


/* ************************************************************
 *
 * CAN
 *
 * ************************************************************/
void MainWindow::on_pushButtonCANTest_clicked()
{
//    quint8      ubType = 0;
//    quint8      ubCanNum=0;
//    QByteArray  array;
//    quint8      ubLen;

    //DATA
//    ubType = ETH_PROTO_TCP_CMD_CAN_TEST;
//    array.push_back(ubType);
//    ubCanNum = ui->comboBoxCAN->currentIndex();
//    array.push_back(ubCanNum);

//    ubLen = 3;
//    buildMsg(ubLen, ETH_PROTO_TCP_CMD_CAN, array);
//    socketTcp.sendData(_protoEthArray);

//    printMessage(MSG_TX, _protoEthArray);
//    waitForExecution(true);
}

void MainWindow::on_pushButtonCANReset_clicked()
{
//    quint8      ubType = 0;
//    quint8      ubCanNum=0;
//    QByteArray  array;
//    quint8      ubLen;

    //DATA
//    ubType = ETH_PROTO_TCP_CMD_CAN_RST;
//    array.push_back(ubType);
//    ubCanNum = ui->comboBoxCAN->currentIndex();
//    array.push_back(ubCanNum);

//    ubLen = 3;
//    buildMsg(ubLen, ETH_PROTO_TCP_CMD_CAN, array);
//    socketTcp.sendData(_protoEthArray);

//    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonCANStatus_clicked()
{
//    quint8      ubType = 0;
//    quint8      ubCanNum=0;
//    QByteArray  array;
//    quint8      ubLen;

    //DATA
//    ubType = ETH_PROTO_TCP_CMD_CAN_STA;
//    array.push_back(ubType);
//    ubCanNum = ui->comboBoxCAN->currentIndex();
//    array.push_back(ubCanNum);

//    ubLen = 3;
//    buildMsg(ubLen, ETH_PROTO_TCP_CMD_CAN, array);
//    socketTcp.sendData(_protoEthArray);

//    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonCANSend_clicked()
{
//    quint8      ubType = 0;
//    quint8      ubCanNum=0;
//    QByteArray  array;
//    quint8      ubLen;

    //DATA
//    ubType = ETH_PROTO_TCP_CMD_CAN_MSG;
//    array.push_back(ubType);
//    ubCanNum = ui->comboBoxCAN->currentIndex();
//    array.push_back(ubCanNum);

//    QString msg = ui->lineEditCANMsg->text();

//    //Store the data message lenght to a future easy use
//    array.push_back(msg.length());
//    //Filling of the array with the data message
//    array.push_back(msg.toUtf8());
//    //Total len is the message+byte lenght+byte section+byte subcommand+byte command
//    ubLen = 4 + msg.length();
//    buildMsg(ubLen, ETH_PROTO_TCP_CMD_CAN, array);
//    socketTcp.sendData(_protoEthArray);

//    printMessage(MSG_TX, _protoEthArray);
}

/* ************************************************************
 *
 * MOTOR
 *
 * ************************************************************/
void MainWindow::on_pushButtonNSHRTest_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NSH_R_TEST;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NSH_R, array);
    socketTcp.sendData(_protoEthArray);

    waitForExecution(true);
    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonNSHRStat_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NSH_R_STAT;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NSH_R, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonNSHRReset_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NSH_R_RST;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NSH_R, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonNSHMTest_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NSH_M_TEST;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NSH_M, array);
    socketTcp.sendData(_protoEthArray);

    waitForExecution(true);
    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonNSHMMove_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;
    qint32      siSteps;
    quint32     uiSteps;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NSH_M_MOVE;
    array.push_back(ubType);
    QString steps =  ui->lineEditNSHMMoveSteps->text();
    siSteps = steps.toInt();

    quint8 direction = 0;
    if (siSteps > 0)
    {
        direction = 1;
        uiSteps = siSteps;
    }
    else
    {
        uiSteps = -siSteps;
    }
    array.push_back(direction);
    array.push_back((quint8)(uiSteps >> 24));
    array.push_back((quint8)(uiSteps >> 16));
    array.push_back((quint8)(uiSteps >> 8));
    array.push_back((quint8)(uiSteps));

    ubLen = 7;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NSH_M, array);
    socketTcp.sendData(_protoEthArray);

    waitForExecution(true);
    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonNSHMReadSteps_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NSH_M_READ;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NSH_M, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonNSHMReadSensor_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NSH_M_SNS;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NSH_M, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonMotorRun_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;
    quint32     uiSpeed;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NSH_M_PRUN;
    array.push_back(ubType);

    quint8 direction = 0;
    if (ui->rBMotorForwardDirection->isChecked())
    {
        direction = 1;
    }
    else if (ui->rBMotorReverseDirection->isChecked())
    {
        direction = 0;
    }
    else
    {
       //Nothing to do here
    }
    QString strSpeed =  ui->lineEditNSHMMotorSpeed->text();
    uiSpeed = strSpeed.toInt();

    array.push_back(direction);
    array.push_back((quint8)(uiSpeed >> 8));
    array.push_back((quint8)(uiSpeed));

    ubLen = 5;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NSH_M, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);

}

void MainWindow::on_pushButtonMotorStop_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NSH_M_STOP;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NSH_M, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pBMotorSoftStart_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NSH_M_SOFT_PRUN;
    array.push_back(ubType);

    quint8 direction = 0;
    if (ui->rBMotorForwardDirection->isChecked())
    {
        direction = 1;
    }
    else if (ui->rBMotorReverseDirection->isChecked())
    {
        direction = 0;
    }
    else
    {
       //Nothing to do here
    }

    array.push_back(direction);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NSH_M, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pBMotorSoftStop_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NSH_M_SOFT_STOP;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NSH_M, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}
/*************************************************************
 *
 * AUDIO
 *
 * ************************************************************/
void MainWindow::readVolume()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_AUDIO_READVOL;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_AUDIO, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::readDAC()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_AUDIO_READDAC;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_AUDIO, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::readDriverGain()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_AUDIO_READGAIN;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_AUDIO, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonAudioPlay_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_AUDIO_PLAY;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_AUDIO, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_verticalSliderVolumeAudio_sliderReleased()
{
    QByteArray  array;
    quint8      ubType = 0;
    quint8      ubLen;
    quint8      ubVolume;

    ubVolume = ui->verticalSliderVolumeAudio->value();

    //DATA
    ubType = ETH_PROTO_TCP_CMD_AUDIO_VOL;
    array.push_back(ubType);
    array.push_back(ubVolume);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_AUDIO, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_verticalSliderVolumeGain_sliderReleased()
{
    QByteArray  array;
    quint8      ubType = 0;
    quint8      ubLen;
    quint8      ubVolume;

    ubVolume = ui->verticalSliderVolumeGain->value();

    //DATA
    ubType = ETH_PROTO_TCP_CMD_AUDIO_GAIN;
    array.push_back(ubType);
    array.push_back(ubVolume);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_AUDIO, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_verticalSliderVolumeDAC_sliderReleased()
{
    QByteArray  array;
    quint8      ubType = 0;
    quint8      ubLen;
    quint8      ubVolume;

    ubVolume = ui->verticalSliderVolumeDAC->value();

    //DATA
    ubType = ETH_PROTO_TCP_CMD_AUDIO_DAC;
    array.push_back(ubType);
    array.push_back(ubVolume);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_AUDIO, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_cBSelection_currentIndexChanged(int index)
{
    QByteArray  array;
    quint8      ubType = 0;
    quint8      ubLen;
//    quint8      ubTrackIndex;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_AUDIO_SETSOUND;
    array.push_back(ubType);
    array.push_back(index);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_AUDIO, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pBStopAudio_clicked()
{
    QByteArray  array;
    quint8      ubType = 0;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_AUDIO_STOP;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_AUDIO, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}
/*************************************************************
 *
 * CAMERA
 *
 * ************************************************************/
void MainWindow::on_pushButtonCameraVersion_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_CAMERA_VRS;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_CAMERA, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonCameraTest_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_CAMERA_TEST;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_CAMERA, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}
/* ************************************************************
 *
 * LED
 *
 * ************************************************************/
void MainWindow::on_pushButtonLEDGreenTest_clicked()
{
    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_LED_GREEN;
    array.push_back(ubType);

    ubInfo = 0x01;

    if (ui->radioButton0Hz8->isChecked())
    {
        ubInfo |= 0x04;
    }
    else if (ui->radioButton1Hz4->isChecked())
    {
        ubInfo |= 0x08;
    }
    else
    {
        //Nothing to do here
    }

    array.push_back(ubInfo);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_LED, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonLEDYellowTest_clicked()
{
    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_LED_YELLOW;
    array.push_back(ubType);

    ubInfo = 0x01;
    if (ui->radioButton0Hz8->isChecked())
    {
        ubInfo |= 0x04;
    }
    else if (ui->radioButton1Hz4->isChecked())
    {
        ubInfo |= 0x08;
    }
    else
    {
        //Nothing to do here
    }
    array.push_back(ubInfo);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_LED, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonLEDRedTest_clicked()
{
    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_LED_RED;
    array.push_back(ubType);

    ubInfo = 0x01;
    if (ui->radioButton0Hz8->isChecked())
    {
        ubInfo |= 0x04;
    }
    else if (ui->radioButton1Hz4->isChecked())
    {
        ubInfo |= 0x08;
    }
    else
    {
        //Nothing to do here
    }
    array.push_back(ubInfo);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_LED, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonLEDGreenTest_2_clicked()
{
    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_LED_GREEN;
    array.push_back(ubType);

    ubInfo = 0x00;
    array.push_back(ubInfo);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_LED, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonLEDYellowTest_2_clicked()
{
    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_LED_YELLOW;
    array.push_back(ubType);

    ubInfo = 0x00;
    array.push_back(ubInfo);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_LED, array);
    socketTcp.sendData(_protoEthArray);

   printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonLEDRedTest_2_clicked()
{
    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_LED_RED;
    array.push_back(ubType);

    ubInfo = 0x00;
    array.push_back(ubInfo);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_LED, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

/* ************************************************************
 *
 * EEPROM
 *
 * ************************************************************/
void MainWindow::on_pushButtonEEpromTest_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_EEPROM_TST;
    array.push_back(ubType);

    array.push_back(ETH_PROTO_TCP_INFO_EEPROM_TEST);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_EEPROM, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

/* ************************************************************
 *
 * NTC
 *
 * ************************************************************/
void MainWindow::on_pushButtonNTC1Test_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NTC_1;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NTC, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonNTC2Test_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NTC_2;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NTC, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonNTC3Test_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NTC_3;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NTC, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_requestTemperature(int val)
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    switch (val)
    {
        case 0:
        default:
            ubType = ETH_PROTO_TCP_CMD_NTC_1;
            break;
        case 1:
            ubType = ETH_PROTO_TCP_CMD_NTC_2;
            break;

        case 2:
            ubType = ETH_PROTO_TCP_CMD_NTC_3;
            break;
    }
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NTC, array);
    socketTcp.sendData(_protoEthArray);
}

void MainWindow::on_pushButtonNTCTest_clicked()
{
    quint8      ubInfo = 0;
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_NTC_TEST;
    array.push_back(ubType);
    ubInfo = ui->comboBoxNTC->currentIndex();
    array.push_back(ubInfo);

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_NTC, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);

}

/* ************************************************************
 *
 * TEC DRIVER
 *
 * ************************************************************/
void MainWindow::on_pB_TECDriverOn_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_TEC_START;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_TEC, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pB_TECDriverOff_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_TEC_STOP;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_TEC, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pB_StartPid_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_TEC_PID_START;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_TEC, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pB_StopPid_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_TEC_PID_STOP;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_TEC, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pB_SetTemp_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_TEC_PID_SETTEMP;
    array.push_back(ubType);


    QString msg = ui->lE_SetTemp->text();

    //Store the data message lenght to a future easy use
    array.push_back(msg.length());
    //Filling of the array with the data message
    array.push_back(msg.toUtf8());
    //Total len is the message+byte lenght+byte section+byte subcommand+byte command
    ubLen = 3 + msg.length();

    buildMsg(ubLen, ETH_PROTO_TCP_CMD_TEC, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

/* ************************************************************
 *
 * HEATER
 *
 * ************************************************************/
void MainWindow::on_pushButtonHeaterEnable_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_PWMDEVICE_ENABLE;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_PWMDEVICE, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonHeaterTest_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_PWMDEVICE_TEST;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_PWMDEVICE, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonHeaterDisable_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_PWMDEVICE_DISABLE;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_PWMDEVICE, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonPwmDeviceSetPeriod_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;
    quint32     uiPeriod;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_PWMDEVICE_PERIOD;
    array.push_back(ubType);

    QString strPeriod =  ui->lineEditPwmDevicePeriod->text();
    uiPeriod = strPeriod.toInt();

    array.push_back((quint8)(uiPeriod >> 24));
    array.push_back((quint8)(uiPeriod >> 16));
    array.push_back((quint8)(uiPeriod >> 8));
    array.push_back((quint8)(uiPeriod));

    ubLen = 6;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_PWMDEVICE, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

void MainWindow::on_pushButtonPwmDeviceDutycycle_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;
    quint8      ubDutyCycle;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_PWMDEVICE_DUTYCYCLE;
    array.push_back(ubType);

    QString strDutyCycle =  ui->lineEditPwmDeviceDutyCycle->text();
    ubDutyCycle = strDutyCycle.toInt();
    array.push_back((quint8)(ubDutyCycle));

    ubLen = 3;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_PWMDEVICE, array);
    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

/* ************************************************************
 *
 * SERIAL
 *
 * ************************************************************/
void MainWindow::on_pushButtonSerialTest_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_SERIAL_TEST;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_SERIAL, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}

/* ************************************************************
 *
 * CONSOLE
 *
 * ************************************************************/
void MainWindow::on_pushButtonConsole_clicked()
{
    if (_console.getActive() == false)
    {
        _console.setGeometry(GEOM_WIDTH+10,50,400,300);
        _console.show();
        _console.setActive(true);
    }
    else
    {
        _console.close();
        _console.setActive(false);
    }
}

void MainWindow::closeEvent (QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, TITLE,
                                                                tr("Are you sure?\n"),
                                                                QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                                                                QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes)
    {
        event->ignore();
    }
    else
    {
        _console.close();
        _console.setActive(false);

        _textresult.setActive(false);
        _textresult.close();

        _voltageD.setActive(false);
        _voltageD.close();

        event->accept();
    }
}

void MainWindow::on_pushButtonTextResult_clicked()
{
    if (_textresult.getActive() == false)
    {
        _textresult.setActive(true);
        _textresult.show();
    }
    else
    {
        _textresult.setActive(false);
        _textresult.close();
    }
}

void MainWindow::on_actionVoltage()
{
    if (_voltageD.getActive() == false)
    {
        _voltageD.setActive(true);
        _voltageD.show();
    }
    else
    {
        _voltageD.setActive(false);
        _voltageD.close();
    }
}

void MainWindow::on_actionTemperature()
{
    if (_tempD.getActive() == false)
    {
        _tempD.setActive(true);
        _tempD.show();
    }
    else
    {
        _tempD.setActive(false);
        _tempD.close();
    }
}

void MainWindow::on_pushButtonReleaseVrs_clicked()
{
    quint8      ubType = 0;
    QByteArray  array;
    quint8      ubLen;

    //DATA
    ubType = ETH_PROTO_TCP_CMD_REL_VRS;
    array.push_back(ubType);

    ubLen = 2;
    buildMsg(ubLen, ETH_PROTO_TCP_CMD_REL, array);

    socketTcp.sendData(_protoEthArray);

    printMessage(MSG_TX, _protoEthArray);
}
