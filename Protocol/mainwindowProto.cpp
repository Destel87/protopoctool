#include <QMessageBox>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "EthProtoInclude.h"
#include "crc32.h"

void MainWindow::readDataClient()
{
    _protoEthReceiveArray.clear();

    QByteArray arrayClient;
    socketTcp.readData(arrayClient);

    printMessage(MSG_RX, arrayClient);
    _protoEthReceiveArray.append(arrayClient);

    parseMsg(_protoEthReceiveArray);

}

void MainWindow::buildMsg(quint16 usLen, quint8 ubCmd, QByteArray arrayData)
{
    _protoEthArray.clear();

    _protoEthArray.push_back(0x02);
    _protoEthArray.push_back((quint8)(usLen >> 8));
    _protoEthArray.push_back((quint8)(usLen));
    _protoEthArray.push_back(ubCmd);
    _protoEthArray.append(arrayData);
    quint32 uiCrc = 0;//TODO

//    unsigned char* hex =new unsigned char[usLen];
    uint8_t hex[200];
    memcpy(hex,_protoEthArray.constData(),usLen+3);
    uiCrc= get32crc(&hex[1],usLen+2);
//    delete [] hex;

    _protoEthArray.push_back((quint8)(uiCrc >> 24));
    _protoEthArray.push_back((quint8)(uiCrc >> 16));
    _protoEthArray.push_back((quint8)(uiCrc >> 8));
    _protoEthArray.push_back((quint8)(uiCrc));
    _protoEthArray.push_back(0x03);
}

void MainWindow::parseMsg(QByteArray arrayMsg)
{
    quint16 usLen=0;
    quint8 ubCmd;
//    quint8 data[255];
    QByteArray dataArray;

    _ethState = ETH_PROTO_STX;
    for (int i =0; i<_protoEthReceiveArray.size(); ++i) {

        switch (_ethState)
        {
        case ETH_PROTO_STX:
            if (_protoEthReceiveArray.at(i)== 0x02)
                _ethState = ETH_PROTO_LEN;
            break;

        case ETH_PROTO_LEN:
            usLen = _protoEthReceiveArray.at(i) << 8;
            usLen |= _protoEthReceiveArray.at(i+1);
            ++i;
            _ethState = ETH_PROTO_CMD;
            break;

        case ETH_PROTO_CMD:
            ubCmd = arrayMsg.at(i);
            _ethState = ETH_PROTO_DATA;
            break;

        case ETH_PROTO_DATA:
            for (int j=0; j<usLen-1;++j) {
                dataArray.append(_protoEthReceiveArray.at(j+4));
            }
            _ethState = ETH_PROTO_CRC;
            break;

        case ETH_PROTO_CRC:
            i = i+4;
            _ethState = ETH_PROTO_ETX;
            break;

        case ETH_PROTO_ETX:
            if (_protoEthReceiveArray.at(i) == 0x03)
               _ethState = ETH_PROTO_STX;
            break;

        default:
            break;
        }

    }

    //Dispatch in order to the received CMD
    switch(ubCmd)
    {
        case ETH_PROTO_TCP_ANS_PM:
            managePMAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_FAN:
            manageFANAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_LED:
            manageLEDAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_CAN:
            manageCANAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_NSH_R:
            manageNSHRAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_NSH_M:
            manageNSHMAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_AUDIO:
            manageAudioAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_CAMERA:
            manageCameraAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_EEPROM:
            manageEEpromAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_NTC:
            manageNTCAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_GPIO:
            manageGPIOAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_PWMDEVICE:
            manageHeaterAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_TEC:
            manageTecAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_SERIAL:
            manageSerialAns(dataArray);
            break;

        case ETH_PROTO_TCP_ANS_REL:
            {
                manageReleaseVersion(dataArray);
            }
            break;

        case ETH_PROTO_TCP_ANS_CONN:
            {
                if (dataArray.at(0) == 0x01)
                {
                    enablePeripheralsBox(true);

                    QPalette pal = ui->pushButtonShutdown->palette();
                    pal.setColor(QPalette::Window, QColor(Qt::green));
                    ui->pushButtonShutdown->setStyleSheet(BACKGND_GREEN);

                    ui->buttonConnect->setEnabled(false);
                    ui->buttonDisconnect->setEnabled(true);
                }
                else
                {
                    on_buttonDisconnect_clicked();
                }

            }
            break;

        default:
            break;
    }
}

void MainWindow::managePMAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
    str.append(data);
    if (subCmd == ETH_PROTO_TCP_CMD_PM_VS)
    {
        ui->lineEditPMRelease->setText(str);
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_PM_READ_DATE)
    {
        QString strCp;
        strCp.append(data.at(1));
        strCp.append(data.at(2));
        strCp.append("/");
        strCp.append(data.at(3));
        strCp.append(data.at(4));
        strCp.append("/");
        strCp.append(data.at(5));
        strCp.append(data.at(6));

        ui->lineEditPMDate->setText(strCp);
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_PM_READ_ID)
    {
        str.remove(0,1);
        ui->lineEditPMIDConf->setText(str);
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_PM_READ_REVISION)
    {
        str.remove(0,1);
        ui->lineEditPMRevision->setText(str);
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_PM_READ_SERIAL)
    {
        str.remove(0,1);
        ui->lineEditPMSerial->setText(str);
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_PM_VOLTAGE)
    {
        str.append("V");
        if (_voltageD.getActive() == true)
        {
            switch (_lVoltageRequested)
            {
                case 1:
                    _voltageD.setV1(str);
                    break;
                case 2:
                    _voltageD.setV2(str);
                    break;
                case 3:
                    _voltageD.setV3(str);
                    break;
                case 4:
                    _voltageD.setV4(str);
                    break;
                case 5:
                    _voltageD.setV5(str);
                    break;
                case 6:
                    _voltageD.setV6(str);
                    break;
                case 7:
                    _voltageD.setV7(str);
                    break;
                case 8:
                    _voltageD.setV8(str);
                    break;
                default:
                    QMessageBox msg;
                    msg.setText("Device not set");
                    msg.exec();
                    break;
            }
        }
        else
        {
            ui->lineEditPMVoltage->setText(str);
        }
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_PM_ENDISV)
    {
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_PM_TURNOFFGW)
    {
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_PM_READGW)
    {
//        if (data.at(0)!= 0)
//        {
//            QPalette pal = ui->labelGWStatus->palette();
//            pal.setColor(QPalette::Window, QColor(Qt::green));
//            ui->labelGWStatus->setPalette(pal);
//        }
//        else
//        {
//            QPalette pal = ui->labelGWStatus->palette();
//            pal.setColor(QPalette::Window, QColor(Qt::red));
//            ui->labelGWStatus->setPalette(pal);
//        }
    }
    else
    {
        QMessageBox msg;
        msg.setText("Device not set");
        msg.exec();
    }
}

void MainWindow::manageFANAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
    str.append(data);
    if (subCmd == ETH_PROTO_TCP_CMD_FAN_STEST)
    {
        if (data.at(0) == 'Y')
        {
            ui->labelFANSingleTestResult->setText(ETH_PROTO_TEST_RESULT_OK);
        }
        else
        {
            ui->labelFANSingleTestResult->setText(ETH_PROTO_TEST_RESULT_NOK);
        }
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_FAN_TACH)
    {
        ui->lineEditTachValue->setText(str);
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_FAN_MTEST)
    {
        if (data.at(0) == 'Y')
        {
            ui->labelFANAllTestResult->setText(ETH_PROTO_TEST_RESULT_OK);
        }
        else
        {
            ui->labelFANAllTestResult->setText(ETH_PROTO_TEST_RESULT_NOK);
        }
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_FAN_RUN)
    {
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_FAN_STOP)
    {
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_FAN_ON)
    {
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_FAN_OFF)
    {
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_FAN_LFD)
    {
    }
    else
    {
        QMessageBox msg;
        msg.setText("Device not set");
        msg.exec();
    }

    waitForExecution(false);
}

void MainWindow::manageLEDAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
//    str.append(data);
    if (subCmd == ETH_PROTO_TCP_CMD_LED_TEST)
    {
        if (data.at(0) == 'Y')
        {
            ui->labelLedTestResult->setText(ETH_PROTO_TEST_RESULT_OK);
        }
        else
        {
            ui->labelLedTestResult->setText(ETH_PROTO_TEST_RESULT_NOK);
        }
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_LED_GREEN)
    {
        ui->labelLedGreenTestResult->setText(data.toHex());

    }
    else if (subCmd == ETH_PROTO_TCP_CMD_LED_YELLOW)
    {
        ui->labelLedYellowTestResult->setText(data.toHex());
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_LED_RED)
    {
        ui->labelLedRedTestResult->setText(data.toHex());
    }
    else
    {
        QMessageBox msg;
        msg.setText("Device not set");
        msg.exec();
    }

    waitForExecution(false);
}

void MainWindow::manageCANAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
    str.append(data);
    if ( subCmd == ETH_PROTO_TCP_CMD_CAN_TEST )
    {
//        if (data.at(0) == 'Y')
//        {
//            ui->labelCANTestResult->setText(ETH_PROTO_TEST_RESULT_OK);
//        }
//        else
//        {
//            ui->labelCANTestResult->setText(ETH_PROTO_TEST_RESULT_NOK);
//        }

    }
    else if (subCmd == ETH_PROTO_TCP_CMD_CAN_RST)
    {
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_CAN_STA)
    {
        int isdata = data.toInt();
        QString string;
        string.append(isdata);
//        ui->lineEditCANStatus->setText(string);
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_CAN_MSG)
    {
    }
    else
    {
        QMessageBox msg;
        msg.setText("Device not set");
        msg.exec();
    }
    waitForExecution(false);
}

void MainWindow::manageNSHRAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
    str.append(data);
    if ( subCmd == ETH_PROTO_TCP_CMD_NSH_R_TEST )
    {
//        if (data.at(0) == 'Y')
//        {
//            ui->labelNSHRTestResult->setText(ETH_PROTO_TEST_RESULT_OK);
//        }
//        else
//        {
//            ui->labelNSHRTestResult->setText(ETH_PROTO_TEST_RESULT_NOK);
//        }

    }
    else if (subCmd == ETH_PROTO_TCP_CMD_NSH_R_STAT)
    {
//        ui->lineEditNSHRStatus->setText(str);
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_NSH_R_RST)
    {
    }
    else
    {
        QMessageBox msg;
        msg.setText("Device not set");
        msg.exec();
    }

    waitForExecution(false);
}

void MainWindow::manageNSHMAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
    str.append(data);
    if ( subCmd == ETH_PROTO_TCP_CMD_NSH_M_TEST )
    {
        if (data.at(0) == 'Y')
        {
            ui->labelNSHMTestResult->setText(ETH_PROTO_TEST_RESULT_OK);
        }
        else
        {
            ui->labelNSHMTestResult->setText(ETH_PROTO_TEST_RESULT_NOK);
        }
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_NSH_M_MOVE)
    {
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_NSH_M_READ)
    {
        ui->lineEditNSHMReadSteps->setText(str);
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_NSH_M_SNS)
    {
        if (data.at(0)== 'Y')
        {
            QPalette pal = ui->labelNSHMSensor->palette();
            pal.setColor(QPalette::Window, QColor(Qt::green));
            ui->labelNSHMSensor->setPalette(pal);
        }
        else
        {
            QPalette pal = ui->labelNSHMSensor->palette();
            pal.setColor(QPalette::Window, QColor(Qt::red));
            ui->labelNSHMSensor->setPalette(pal);
        }
    }
    else if ((subCmd == ETH_PROTO_TCP_CMD_NSH_M_PRUN) || (subCmd == ETH_PROTO_TCP_CMD_NSH_M_SOFT_PRUN))
    {
    }
    else if ((subCmd == ETH_PROTO_TCP_CMD_NSH_M_STOP) || (subCmd == ETH_PROTO_TCP_CMD_NSH_M_SOFT_STOP))
    {
    }
    else
    {
            QMessageBox msg;
            msg.setText("Device not set");
            msg.exec();
    }
    waitForExecution(false);
}

void MainWindow::manageAudioAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
    str.append(data);
    if ( subCmd == ETH_PROTO_TCP_CMD_AUDIO_VOL )
    {
        ui->labelAudioVolume->setText(data);
    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_AUDIO_PLAY )
    {
    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_AUDIO_READVOL )
    {
        ui->labelAudioVolume->setText(data);
        ui->verticalSliderVolumeAudio->setSliderPosition(data.toInt());

        readDAC();
    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_AUDIO_DAC )
    {
        ui->labelAudioDAC->setText(data);
    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_AUDIO_READDAC )
    {
        ui->labelAudioDAC->setText(data);
        ui->verticalSliderVolumeDAC->setSliderPosition(data.toInt());

        readDriverGain();
    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_AUDIO_GAIN )
    {
        ui->labelAudioDriverGain->setText(data);
    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_AUDIO_READGAIN )
    {
        ui->labelAudioDriverGain->setText(data);
        ui->verticalSliderVolumeGain->setSliderPosition(data.toInt());
    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_AUDIO_SETSOUND )
    {

    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_AUDIO_STOP )
    {

    }
    else
    {
        QMessageBox msg;
        msg.setText("Device not set");
        msg.exec();
    }
}

void MainWindow::manageCameraAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
    str.append(data);
    if ( subCmd == ETH_PROTO_TCP_CMD_CAMERA_VRS )
    {
        ui->lineEditCameraVersion->setText(str);
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_CAMERA_TEST)
    {
        if (data.at(0) == 'Y')
        {
            ui->labelCameraTest->setText(ETH_PROTO_TEST_RESULT_OK);
        }
        else
        {
            ui->labelCameraTest->setText(ETH_PROTO_TEST_RESULT_NOK);
        }
    }
    else
    {
        QMessageBox msg;
        msg.setText("Device not set");
        msg.exec();
    }

}

void MainWindow::manageEEpromAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
    str.append(data);
    if ( subCmd == ETH_PROTO_TCP_CMD_EEPROM_TST )
    {
        char appo = data.at(0);
        int resTest = atoi(&appo);
        if (resTest == ETH_PROTO_TCP_INFO_EEPROM_TEST)
        {
            ui->labelEEpromTestResult->setText(ETH_PROTO_TEST_RESULT_OK);
        }
        else
        {
            ui->labelEEpromTestResult->setText(ETH_PROTO_TEST_RESULT_NOK);
        }
    }
    else
    {
        QMessageBox msg;
        msg.setText("Device not set");
        msg.exec();
    }

}

void MainWindow::manageNTCAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
    str.append(data);
    if ( subCmd == ETH_PROTO_TCP_CMD_NTC_1 )
    {
        str.append("°C");
        if (_tempD.getActive() == true)
        {
            _tempD.setTemp(str);
        }
        else
        {
            ui->lineEditNTC1Value->setText(data);
        }
    }
    else if (subCmd == ETH_PROTO_TCP_CMD_NTC_2)
    {
        str.append("°C");
        if (_tempD.getActive() == true)
        {
            _tempD.setTemp(str);
        }
        else
        {
           ui->lineEditNTC2Value->setText(data);
        }

    }
    else if (subCmd == ETH_PROTO_TCP_CMD_NTC_3)
    {
        str.append("°C");
        if (_tempD.getActive() == true)
        {
            _tempD.setTemp(str);
        }
        else
        {
            ui->lineEditNTC3Value->setText(data);
        }
    }
    else if(subCmd == ETH_PROTO_TCP_CMD_NTC_TEST)
    {
        if (data.at(0) == 'Y')
        {
            ui->labelNTCTestResult->setText(ETH_PROTO_TEST_RESULT_OK);
        }
        else
        {
            ui->labelNTCTestResult->setText(ETH_PROTO_TEST_RESULT_NOK);
        }
    }
    else
    {
        QMessageBox msg;
        msg.setText("Device not set");
        msg.exec();
    }
}

void MainWindow::manageGPIOAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
    str.append(data);
    if ( subCmd == ETH_PROTO_TCP_CMD_GPIO_SHD )
    {
        if (data.at(0)!= 0)
        {
            QPalette pal = ui->pushButtonShutdown->palette();
            pal.setColor(QPalette::Window, QColor(Qt::red));
            ui->pushButtonShutdown->setStyleSheet(BACKGND_RED);
        }
        else
        {

        }
    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_GPIO_CONF ) {
        quint8 pinHwConf0;
        quint8 pinHwConf1;

        pinHwConf0 = data.at(0) & 0x01;
        pinHwConf1 = (data.at(0) & 0x02) >>1;

        ui->verticalSliderHWConf0->setValue(pinHwConf0);
        ui->verticalSliderHWConf1->setValue(pinHwConf1);
    }
    else {
        QMessageBox msg;
        msg.setText("Device not set");
        msg.exec();
    }
}

void MainWindow::manageTecAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
    str.append(data);

    if ( subCmd == ETH_PROTO_TCP_CMD_TEC_START )
    {
        _TECDriverEnabled = true;
    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_TEC_STOP )
    {
        _TECDriverEnabled = false;
    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_TEC_PID_START )
    {
        _PIDEnabled = true;
    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_TEC_PID_STOP )
    {
        _PIDEnabled = false;
    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_TEC_PID_SETTEMP )
    {

    }
    else
    {
        QMessageBox msg;
        msg.setText("Device not set");
        msg.exec();
    }

    //Update TEC label
    if (_TECDriverEnabled == true)
    {
        if (_PIDEnabled == true)
        {
            ui->label_TECenabled->setText("TEC ENABLED PID ON");
        }
        else
        {
            ui->label_TECenabled->setText("TEC ENABLED PID OFF");
        }
    }
    else
    {
        ui->label_TECenabled->setText("TEC DISABLED PID OFF");
    }

}

void MainWindow::manageHeaterAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
    str.append(data);
    if ( subCmd == ETH_PROTO_TCP_CMD_PWMDEVICE_TEST )
    {
        if (data.at(0) == 'Y')
        {
            ui->labelHeaterTestResult->setText(ETH_PROTO_TEST_RESULT_OK);
        }
        else
        {
            ui->labelHeaterTestResult->setText(ETH_PROTO_TEST_RESULT_NOK);
        }
    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_PWMDEVICE_ENABLE )
    {

    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_PWMDEVICE_DISABLE )
    {

    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_PWMDEVICE_PERIOD )
    {

    }
    else if ( subCmd == ETH_PROTO_TCP_CMD_PWMDEVICE_DUTYCYCLE )
    {

    }
    else
    {
        QMessageBox msg;
        msg.setText("Device not set");
        msg.exec();
    }
}

void MainWindow::manageSerialAns(QByteArray data)
{
    QString str;
    quint8 subCmd = data.at(0);
    data.remove(0,1);
    str.append(data);
    if ( subCmd == ETH_PROTO_TCP_CMD_SERIAL_TEST )
    {
//        if (data.at(0) == 'Y')
//        {
//            ui->labelSerialTestResult->setText(ETH_PROTO_TEST_RESULT_OK);
//        }
//        else
//        {
//            ui->labelSerialTestResult->setText(ETH_PROTO_TEST_RESULT_NOK);
//        }
    }
    else
    {
        QMessageBox msg;
        msg.setText("Device not set");
        msg.exec();
    }
}

void MainWindow::manageReleaseVersion(QByteArray data)
{
    QString str;
//    quint8 subCmd = data.at(0);
//    data.remove(0,1);
    str.append(data);
    ui->labelReleaseVersion->setText(data);
}
