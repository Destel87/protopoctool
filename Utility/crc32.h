/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    crc32.h
 @author  BmxIta FW dept
 @brief   Contains the functions for the CRC32 algorithm.
 @details

 ****************************************************************************
*/

#ifndef __CRC32_H__
#define __CRC32_H__

#ifdef __cplusplus  /* extern "C" */
extern "C" {
#endif

#define CRC32_INITVALUE 0xFFFFFFFFL
#define CRC32_XORVALUE 0xFFFFFFFFL

extern unsigned long  get32crc(const unsigned char *buf, int len);
void calcrc32(unsigned long *pCrc32,const unsigned char pdata);

#ifdef __cplusplus  /* extern "C" */
}
#endif


#endif /* __CRC_H__ */
